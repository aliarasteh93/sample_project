from peewee import *
from setting import Setting


class SampleModel(Model):
    id = PrimaryKeyField()
    name = CharField(max_length=50, null=False, unique=True)

    class Meta:
        database = Setting().MyDB