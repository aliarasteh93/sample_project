import platform


class Setting():
    def __init__(self):
        if platform.system() == "Windows":
            self.MyDB = ''
            self.ServerPath = ""
            self.File_Address = "C:\Users\Ali\workspace\drugmanagement"
            self.Split = "\\"
            self.db_sql_pass = ""
            self.db_sql_name = ""
            self.db_sql_user = "root"
            self.ip = '127.0.0.1'
            self.port = '8001'
        else:
            self.MyDB = ''
            self.ServerPath = "/var/drugmanagement/"
            self.File_Address = "/var/drugmanagement/"
            self.Split = "/"
            self.db_sql_pass = ""
            self.db_sql_name = ""
            self.db_sql_user = "root"
            self.ip = ''
            self.port = '800'