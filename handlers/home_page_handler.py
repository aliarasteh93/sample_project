import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


class HomePageHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("home.html")

    def post(self):
        pass
