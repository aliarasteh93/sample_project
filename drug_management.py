import tornado
import os
from urls import url_patterns
from tornado.options import options, define

define("port", default=8888, help="run on the given port", type=int)


class DrugManagementApplication(tornado.web.Application):
    def __init__(self):
        handlers = url_patterns
        settings = dict(
            debug=True,
            autoreload=True,
            cookie_secret="61oETz3455545gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            **{
                'pycket': {
                    'engine': 'redis',
                    'storage': {
                        'host': 'localhost',
                        'port': 6379,
                        'db_sessions': 10,
                        'db_notifications': 11,
                        'max_connections': 2 ** 31,
                    },
                    'cookies': {
                        'expires_days': 120,
                    },
                },
            }

        )
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(DrugManagementApplication())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()